#!/bin/bash

# This script resize the flags from the 640x480 directory. A new
# directory with the new dimensions is created. If this directory
# already exists it will be deleted in advance.
#
# Usage: ./resize.sh <width> <height>

directory="$1x$2"
rm -rf $directory
mkdir $directory

for f in ./640x480/*.svg; do
    flag=`basename $f`
    echo "Processing flag $flag ..."
    rsvg-convert -w $1 -h $2 -f svg -o "$directory/$flag" $f
    pngfile=`basename -s .svg $flag`
    pngfile=$directory/$pngfile".png"
    rsvg-convert -w $1 -h $2 -f png -o $pngfile $f
done
